# Hello World

This is my first repo on GitLab :)

That's cool!

## Some usefull commands for git:

If you need to rollback to a precedent commit (for example 582b87b7b682115ea2bd5f11d182de33fcb07222):

```
git checkout 582b87b7b682115ea2bd5f11d182de33fcb07222 .
```

Don't forgive the '.' at the end of the command. You can find the number id of commit in the commits list or with:

```
git log
```

Se vuoi fare il discard di un file dallo staging (unstage) di un file, prima del commit, quindi hai fatto già l'add:

```
git reset HEAD nomefile
```

oppure

```
git reset HEAD .
```

adesso se vuoi ripristinare il file rispetto alle modifiche (scartandole) fai(in pratica se non hai fatto l'add fai direttamente questo):

```
git checkout -- nomefile
```
 
ATTENZIONE: Se vuoi fare il discard di tutte le cose che hai fatto fino adesso e prima del push ma dopo il commit (ATTENZIONE PERDI MATERIALMENTE I FILE/CARTELLE):

```
git reset --hard @{u}
```

Se volessi fare l'unstage delle modifiche, cioè ho fatto delle modifiche, ho fatto l'add e magari il commit ma non ancora il push e voglio tornare all'unstage (cioè al passo precedente l'add) e quindi SENZA PERDITA DEI FILE:

```
git reset HEAD~
```

Per cambiare il remote dopo un clone:

```
git remote -v
git remote set-url origin https://github.com/...
```

e posso cambiare nome della cartella dove ho clonato con mv.

Per eliminare tutti i cambiamenti e i commit fatti in locale e recuperare l'ultima versione sul server GIT facendo puntare il tuo master branch a quello del server:

```
git fetch origin
git reset --hard origin/master
```

Per creare un nuovo branch e passare a lavorare su questo branch scrivo:

```
git checkout -b nome_branch
```

Per farne il merge su master e cancellare il branch:

```
git checkout master
git merge master nome_branch
git branch -d nome_branch
```

se volessi cancellare un branch non unito al master:

```
git branch -D nome_branch
```

e per cancellarlo in remoto:

```
git push --delete origin nome_branch
```

## Others

Se ho dei conflitti, avrò un file (o più files) con:

```
If you have questions, please
<<<<<<< HEAD
open an issue
=======
ask your question in IRC.
>>>>>>> branch-a
```

Togli le righe:

```
<<<<<<< HEAD

=======
>>>>>>> branch-a
```

e poi fai l'add e il push :)